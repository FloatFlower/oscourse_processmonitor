<?php

  require("monitor.php") ;

  if ( ( $pid1 = pcntl_fork() ) === 0 )
  {
    posix_setsid() ;
    if( ( $pid2 = pcntl_fork() ) === 0 )
    {
      $monitor = new Monitor() ;
      $monitor->start() ;
    }
    else
    {
      exit ;
    }
    pcntl_wait($status) ;
  }
?>