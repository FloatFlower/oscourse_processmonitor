<?php
class Process
{
  public function __construct()
  {

  }

  public function setUserName( $userName )
  {
    $this->m_userName = $userName ;
  }

  public function setPid( $pid )
  {
    $this->m_pid = $pid ;
  }

  public function setStartTime( $startTime )
  {
    $this->m_startTime = $startTime ;
  }

  public function setProcessName( $processName )
  {
    $this->m_processName = $processName ;
  }

  public function userName()
  {
    return $this->m_userName ;
  }

  public function pid()
  {
    return $this->m_pid ;
  }

  public function startTime()
  {
    return $this->m_startTime ;
  }

  public function processName()
  {
    return $this->m_processName ;
  }
  private $m_userName ;
  private $m_pid ;
  private $m_startTime ;
  private $m_processName ;
}
?>