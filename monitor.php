<?php

require("process.php") ;
require("log-writer.php") ;

class Monitor
{
  public function __construct(){
    $this->m_logWriter = new LogWriter() ;
  }
  public function start(){
    
    while(true)
    {
      $currentProcessRawData = array() ;
      exec("ps aux",$currentProcessRawData) ;
      for( $i = 1 ; $i < count( $currentProcessRawData ) ; $i ++ )
      {
        $parts = explode( " " , $currentProcessRawData[$i] ) ;
        $index = 0 ;
        $currentProcess = new Process ;
        $processName = "" ;
        $currentPid = "" ;

        for( $j = 0 ; $j < count( $parts ) ; $j++ )
        {
          if ( $parts[$j] == "" ) continue ;
          else $index ++ ;

          switch( $index )
          {
            case 1 :
              // user name
              $currentProcess->setUserName( $parts[$j] );
              break ;
            case 2 :
              // pid
              $currentProcess->setPID( $parts[$j] ) ;
              $currentPid = $parts[$j] ;
              break ;
            case 9 :
              // start time
              $currentProcess->setStartTime( $parts[$j] ) ;
              break ;
            default :
              // app name
              if( $index >= 11 )
              {
                $processName = $processName." ".$parts[$j] ;
              }
          }
          $currentProcess->setProcessName($processName) ;
          $this->currentProcessList[$currentPid] = $currentProcess ;
        } // for $j
      } // for $i

      $this->lastProcessList = $this->aliveProcessList ;
      $this->aliveProcessList = array() ;
      foreach ( $this->currentProcessList as $key=>$value )
      {
        if( isset( $this->lastProcessList[$key] ) )
        {
          $this->aliveProcessList[$key] = $value ;
          unset($this->lastProcessList[$key]) ;
          unset($this->currentProcessList[$key]) ;
        }
        else
        {
          $this->aliveProcessList[$key] = $value ;
          
          $info = array() ;
          $info["userName"] = $value->userName() ;
          $info["PID"] = $value->startTime() ;
          $info["startTime"] = $value->startTime() ;
          $info["processName"] = $value->processName() ;
          $this->m_logWriter->createProcessMessage($info) ;
          //print_r($info) ;
          unset($this->currentProcessList[$key]) ;

        }
      }
      if ( count($this->lastProcessList) != 0 ) {
        foreach ( $this->lastProcessList as $value )
        {
          $closedProcess = $value ;
          $info = array() ;
          $info["userName"] = $closedProcess->userName() ;
          $info["PID"] = $closedProcess->pid() ;
          $currentTime = localtime() ;
          $info["endTime"] = $currentTime[2].":".$currentTime[1] ;
          $info["processName"] = $currentProcess->processName() ;
          $this->m_logWriter->closeProcessMessage($info) ;
        }
      }
      sleep(1) ;
    } // while true 
  } // start()
  private $currentProcessList ;
  private $aliveProcessList ;
  private $lastProcessList ;
  private $m_logWriter ;
}

?>