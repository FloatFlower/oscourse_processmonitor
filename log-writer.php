<?php
class LogWriter
{
  public function __construct()
  {

  }

  public function createProcessMessage( $info )
  {
    $userName = $info["userName"] ;
    $pid = $info["PID"] ;
    $startTime = $info["startTime"] ;
    $appName = $info["processName"] ;

    $msg = "[ProcessCreate] User : ".$userName.", PID : ".$pid.", StartTime : ".$startTime.", AppName : ".$appName."\n" ;
  
    $this->writeLog( $msg );

  }

  public function closeProcessMessage( $info )
  {
    $userName = $info["userName"] ;
    $pid = $info["PID"] ;
    $endTime = $info["endTime"] ;
    $appName = $info["processName"] ;

    $msg = "[ProcessClosed] User : ".$userName.", PID : ".$pid.", EndTime : ".$endTime.", AppName : ".$appName."\n" ;
  
    $this->writeLog( $msg );
  }

  private function writeLog ( $msg )
  {
    $fileName = getdate();
    $fileNameFullName = "/var/process_record/".$fileName["year"]."-".$fileName["mon"]."-".$fileName["mday"].".log" ;
    $logfile = fopen($fileNameFullName,"a+") ;
    fwrite($logfile,$msg) ;
  }
}
?>